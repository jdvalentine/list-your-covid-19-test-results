from imap_tools import MailBox, AND
import toml
import datetime
from icecream import ic
import re

"""Load configuration from .toml file."""
config = toml.load('config.toml')
email = config['email']


# Server is the address of the imap server
mb = MailBox(email['server']).login(email['username'], email['password'])

all_processed_msgs = []

for msg in mb.fetch(
                    AND(date_gte=datetime.date(2020, 1, 3), from_='nhs.covid19.notification@notifications.service.gov.uk'),
                    mark_seen=False, bulk=True):


    processed_msg = {}
    processed_msg['sent_date'] = msg.date.strftime('%Y-%m-%d %H:%M:%S')
    processed_msg['person_name'] = re.search(r'Dear (.*?)\r', msg.text).group(1)
    processed_msg['birth_date'] = re.search(r'Birth date: (.*?)\r', msg.text).group(1)
    processed_msg['test_date'] = re.search(r'Test date: (.*?)\r', msg.text).group(1)

    if (re.search(r'is negative', msg.text)):
        processed_msg['test_result'] = 'negative'
    elif (re.search(r'is positive', msg.text)):
        processed_msg['test_result'] = 'POSITIVE'
    
    if (re.search(r'lateral flow', msg.text)):
        processed_msg['test_type'] = 'lateral flow'
    elif (re.search(r'PCR .*or other lab test.* result', msg.text)):
        processed_msg['test_type'] = 'pcr'
        
    all_processed_msgs.append(processed_msg)


for pmsg in all_processed_msgs:
    print(config['output']['delimiter'].join(pmsg.values()))


	
        
        
 




