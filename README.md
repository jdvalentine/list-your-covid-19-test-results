# List your COVID-19 Test Results

Extract all the NHS Test and Trace COVID-19 test results from an IMAP mailbox and tabulate them for pasting into a spreadsheet or whatever. Play at your own risk. This code is not endorsed by the UK Gov't or the NHS (of course).

It represents a little over one hour's work. I may extend this but it is meeting my needs as a proof of concept for a client at present.

## Requirements

 - Python 3+ (using 3.9 in dev)
 - Dependencies as per Pipfile
 - [pipenv](https://pipenv.pypa.io/en/latest/install/) (or use the requirements.txt included and go your own way)

## Installation

 - Clone the repo
 - run ```pipenv shell```

## Use

 - Save a copy of config.sample.toml as config.toml
 - Enter your own credentials into config.toml
 - run ```pipenv shell``` (if you haven't already) to set up the virtual environment
 - run readmail.py with the Python interpreter, probably with ```python readmail.py```

## Roadmap

 - In its present form, this is only of interest to coders who can grok the source and run the tool locally.
 - It should definitely be a module! And have a main method!
 - I think it would be worth improving the output (beyond the customisable delimiter)
 - Wrapping this in a web service may be viable and I may take that approach if I can mitigate the security concerns users might legitimately have.

## Licence
Apache 2.0

## Contributing

This is an open source project in its very early stages and I am very happy to accept community contributions as my time allows. Please submit pull requests. If you are considering extending this for your own use I have no way of policing your choice to "redevelop" based on it. I would however be grateful if you would feed back to me so that I can extend this version for the benefit of less capable developers.

